##################################################################################
# ECS: CLUSTER, SERVICE & TASK DEFINITION
##################################################################################

resource "aws_ecs_cluster" "my-cluster" {
  name = var.cluster_name
}

data "template_file" "task" {
  template = "${file("task-definitions/service.json")}"
  vars = {
    cpu = "1024"
    app_port = "8080"
    mem = "1024"
    reserved = "1024"
    app_name = "${var.app_name}"
    ecr_image = "509130302659.dkr.ecr.sa-east-1.amazonaws.com/dev/my-java-app:v1.0"
    env = "${var.env}"
    region = "${var.region}"
    #add control var to may reload the task definition
    
  }
}


# TASK DEF CONFIGURATION - ECS TASK SETUP
resource "aws_ecs_task_definition" "app" {
  family                = "${var.app_name}-${var.env}"
  #task_role_arn = aws_iam_role.ecs_task_execution_role.arn
  #execution_role_arn = aws_iam_role.ecs_task_execution_role.arn
  #task_role_arn = "arn:aws:iam::509130302659:role/ecs_role"
  execution_role_arn = aws_iam_role.ecsTaskExecutionRole.arn
  container_definitions = data.template_file.task.rendered
  #container_definitions = file("task-definitions/service.json")
  
}

resource "aws_ecs_service" "app" {
  name            = "${var.app_name}-${var.env}-service"
  cluster         = aws_ecs_cluster.my-cluster.id
  task_definition = aws_ecs_task_definition.app.arn
  desired_count   = "1"
  launch_type            = "EC2"   
  #iam_role        = aws_iam_role.ecs_task_execution_role-2.name

  ordered_placement_strategy {
    type  = "binpack"
    field = "cpu"
  }

  load_balancer {
    target_group_arn  = aws_lb_target_group.targetgrp-app.arn
    container_name = "${var.app_name}-${var.env}"
    container_port = "8080"
  }
  depends_on = [aws_lb_listener.front_end]
}