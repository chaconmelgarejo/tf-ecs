#!/bin/bash

# Update all packages

yum update -y
yum install -y ecs-init
service docker start
start ecs

#Adding cluster name in ecs config
echo ECS_CLUSTER=${ecs_cluster} >> /etc/ecs/ecs.config
cat /etc/ecs/ecs.config | grep "ECS_CLUSTER"