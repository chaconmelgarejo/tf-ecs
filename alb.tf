##################################################################################
# LOAD BALANCERS & TGG
##################################################################################

resource "aws_lb" "my-alb" {
  name               = "${var.env}-alb"
  internal           = false
  load_balancer_type = "application"
  subnets         = [aws_subnet.subnet1.id, aws_subnet.subnet2.id]
  security_groups = [aws_security_group.alb-sg.id]

  enable_deletion_protection = false

  # access_logs {
  #   bucket  = aws_s3_bucket.lb_logs.bucket
  #   prefix  = "test-lb"
  #   enabled = true
  # }

  tags = merge(local.common_tags, { Name = "${var.env}-alb" })
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.my-alb.arn
  port              = "80"
  protocol          = "HTTP"
  #ssl_policy        = "ELBSecurityPolicy-2016-08"
  #certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.targetgrp-app.arn
  }
}

# TARGET GROUP SETUP #
resource "aws_lb_target_group" "targetgrp-app" {
  name     = "tg-${var.app_name}-${var.env}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc.id
  health_check {
    healthy_threshold   = 5
    interval            = 30
    path                = "/"
    timeout             = 10
    unhealthy_threshold = 2
  }
  # stickiness {
  #   type            = "lb_cookie"
  #   cookie_duration = 900
  #   enabled         = true
  # }
}